import React, {useState, useEffect} from 'react';
// import './App.css';

const META = {
  children:[
  {
    name:"Marketing & Sales",
    weightPercent:50,
    isRoot:true,
    children:[
      {
        name:"Marketing",
        weightPercent:25,
        children:[
          {
            name:"Digital Marketing",
            weightPercent:25,
            children:[]
          }
        ]
      },
        {
          name:"Sales",
          weightPercent:25,
          children:[]
        }
    ]
  },
  {
    name:"Finance",
    weightPercent:25,
    isRoot:true,
    children:[
      {
        name:"Inhouse Expenses",
        weightPercent:15,
        children:[]
      },
      {
        name:"Tax Audit",
        weightPercent:10,
        children:[]
      }
    ]
  },
  {
    name:"Operation & Maintenance",
    weightPercent:25,
    isRoot:true,
    children:[
      {
        name:"Operation",
        weightPercent:13,
        children:[
          {
            name:"Internal Operation",
            weightPercent:13,
            children:[]
          }
        ]
      },
      {
        name:"Maintenance",
        weightPercent:12,
        children:[]
      }
    ]
  }
]
}



const InputFund = props =>{
  const [amt, setAmt] = useState()
  return (
        <div>
          Enter Allocated Total Amount
          <input type="number" min ={1} step= {1} value = {amt} onChange={e => setAmt(parseInt(e.target.value))}/>
          <button onClick={e=>props.updateAmt(amt) }> 
            Submit
          </button>
        </div>
        )
}

const MainContainerStyle = {
  border:"1px solid #aaa",
  borderRadius:"10px",
}

const MainContainer = props =>{
  return (
      <div style={MainContainerStyle}>
        {props.msg || "" }
      </div>
    )
}



const MainDisplay = props =>{
  let data = props.data
  const [amt, setAmt] = useState(data["weightPercent"])
  const [baseAmt, setBaseAmt] = useState()
  const [updatedDiff, setUpdatedDiff] = useState(0)

  const update = (val) =>{ 
    console.log(val)  
  }
  useEffect(()=>{
    if(baseAmt === undefined){
      setBaseAmt(data["weightPercent"])
    }

  },[])


  const recalculateRootWeights = (val) =>{
    let _amt = amt
    if(baseAmt===undefined){
      _amt = baseAmt
    }
    setAmt(parseInt(_amt) + parseInt(val))
    console.log("relcalculate")
  }

  const updateFromChild=(val) =>{
    // console.log(data["name"])
    setAmt((parseInt(amt) + val).toString())
    setBaseAmt(amt)
    if(data["isRoot"]){
      recalculateRootWeights()
    }else{
      props.updateFromChild(val)
    }
    
  }

  const getPanel = () =>{
    return (
      <div>
        <div>
          {data["name"]}
          <input 
            type="number" 
            min = {1} 
            value={amt} 
            onChange={e=> {
                setAmt(e.target.value)
              }
            }
            onBlur={ e=> {
              let diff = parseInt(e.target.value) - parseInt(baseAmt)
              // console.log(diff)
              data["isRoot"] ? recalculateRootWeights(diff) : props.updateFromChild(diff) 
              setBaseAmt(amt)
            }
            }
          />
          </div>
          <div style={{marginLeft:props.level*50+"px"}}>
            {getChildren()}
          </div>
      </div>
    )
  }

  const getChildren =() =>{
    return Object.keys(data).includes("children")?
            data["children"].map((row, index)=>(
              <div key={index}>
                <MainDisplay data={row} totalAmount = {props.totalAmount} level={props.level + 1} updateFromChild={e=> updateFromChild(e)}/>
              </div>
            )):null
  }

  return Object.keys(data).includes("name")? getPanel() : getChildren()
}




function App(props) {
  const [amt, setAmt] = useState()
  


  return (
    <div className="App">
        <div>
          FUND ALLOCATION
        </div>
        <InputFund updateAmt = {e=> setAmt(e)} />
        {
          (amt===undefined)?
           <MainContainer msg = "Please Enter Amount Allocated!"/>
           :(
              <MainDisplay data = {META} totalAmount = {amt} level={0}/>
           )
           
        }

    </div>
  );
}

export default App;
